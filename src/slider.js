import React, { useRef, useState } from "react";

const Slide = ({ bg, children }) => {
    return (
        <div className="slider__slide">
            <div className="slider__slide_content" style={bg ? { backgroundImage:`url(${bg})` } : null}>
                {children}
            </div>
        </div>
    )
}

const Slider = ({ children, showNav, startFrom }) => {
    const slider = useRef();
    const [activeSlide, setActiveSlide] = useState(startFrom ? startFrom : 0);

    const showPrevSlide = () => {

    }

    const showNextSlide = () => {

    }

    const updateNavElements = (index) => {

    }

    return(
        <div className="slider-wrapper">
            <div className="slider" ref={slider}>
                {children}
            </div>

            <div className="slider-wrapper__buttons">
                <button onClick={showPrevSlide} disabled={activeSlide ===0}> Prev </button>
                <button onClick={showNextSlide} disabled={activeSlide === children.length - 1}> Next </button>
            </div>

            {showNav && <div className="slider-wrapper__nav">
                {children.map((s, i) => <span key={i} className={activeSlide === i ? 'active' : ''} onClick={() => updateNavElements (i)}>
                    </span>)}
                </div>}
        </div>

    )
}

export { Slide, Slider };
